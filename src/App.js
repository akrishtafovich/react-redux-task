import React, { Component } from 'react';
import { connect } from 'react-redux';

import ProductForm from './components/ProductForm';
import ProductList from './components/ProductList';
import ProductDetail from './components/ProductDetail';
import 'uikit/dist/css/uikit.min.css';
import './App.css';

import { Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    console.log(this.props)
    return (
        <div className="row">
          <ProductForm />
          <Switch>
            <Route exact path='/' render={() => (
              <div className="products">
                <ProductList />
                { this.props.productStore.products.length > 0 ?
                    <span className="total-count">
                      Total {this.props.productStore.products.reduce( (sum, el) =>  (sum += el.price * el.count), 0)} $
                    </span> :
                    null
                }
             </div>
            )}/>
            <Route path='/:id' component={ProductDetail}/>
          </Switch>
        </div>
    );
  }
}
export default connect(
  (state, ownProps) => ({
    productStore: state,
    ownProps
  }),
  dispatch => ({

  })
)(App);
