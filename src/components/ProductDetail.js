import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class ProductDetail extends Component {
  render()
   {
    const product = this.props.product;
    return(
      <div className="column" >
        <h3>{product.name}</h3>
        <div className="product-detail">
          <img src={product.image}/>
          <span>Count: {product.count}</span>
          <span>Price: {product.price} $</span>
          <span>Total: {product.total} $</span>
          <Link to={'/'} className="uk-button bg-custom-dblue ">Back to list</Link>
        </div>
      </div>
    );
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    product: state.products.find(product => product.id === Number(ownProps.match.params.id))
  };
}

export default connect(mapStateToProps)(ProductDetail);
