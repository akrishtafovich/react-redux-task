import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Increment extends Component {
  constructor(props){
      super(props);
      this.state = {
        counter: this.props.value ? this.props.value : 1
      }
      this.setTotal()
    }

    setTotal() {
      if(this.props.id){
        return this.props.updateCounter(this.props.id,this.state.counter)
      }else if(this.props.setCount){
        return this.props.setCount(this.state.counter)
      }
    }

    increment = () => {
      this.setState({
        counter: ++this.state.counter
      });
      this.setTotal();
    };

    decrement = () => {
      this.setState({
        counter: this.state.counter === 1 ? 1 : --this.state.counter
      });
      this.setTotal();
    };

    reset = () => {
      this.setState({
        counter: 1
      });
    }

  render() {
    return (
        <div>
          <div className = "uk-margin uk-inline-block" >
            <input
                    type="button"
                    className="uk-button bg-custom-dblue count-btn"
                    onClick={this.decrement} value="-"/>
          <div className="uk-inline-block count-state cl-white">{ this.state.counter }</div>
            <input
                    type="button"
                    className="uk-button bg-custom-dblue count-btn"
                    onClick={this.increment} value="+"/>
          </div>
        </div>
    );
  }
}

export default Increment;
