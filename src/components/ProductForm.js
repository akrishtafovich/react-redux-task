import React, { Component } from 'react';
import { connect } from 'react-redux';
import Counter from './Counter';
import LazyLoad from 'react-lazy-load';
import ImageList from './ImageList';

class ProductForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      price: '',
      name: '',
      toggleImage: false,
      currentImage: require('../img/basket.png')
    };
  }

  addProduct = () => {
    const payload =  {
      id: Date.now(),
      name: this.state.name,
      price: Number(this.state.price),
      count: this.state.counter,
      total: Number(this.state.price) * this.state.counter,
      image: this.state.currentImage
    }

    this.props.onAddProduct(payload);
    this.resetForm();
  }

  handleImage = (img) => {
    this.setState({
      currentImage: require('../img/' + img + '.png'),
      toggleImage: false
    })
  }

  toggleImageList = () => {
    this.setState({
      toggleImage: true
    });
  }

  setCount = (counter) =>{
    this.setState({
      counter: counter
    });
  }

  resetForm = () => {
    this.setState({
      price: '',
      name: '',
      currentImage: require('../img/basket.png')
    });
    this.refs.counter.reset();
  }

  render() {
    let { name, price } = this.state;
    let isEnabled =
      name.length && price.length ;
    return (
      <div className="column">
      <h3>Add product to you card list</h3>
      <div className="uk-padding-small uk-padding-remove-vertical">
        < form >
            <div className="uk-margin">
                <input  placeholder = "Product name"
                        type = "text"
                        className="uk-input bg-custom-dblue"
                        value={ this.state.name }
                        onChange={ (e) => { this.setState({ name: e.target.value }) } }
                        />
              </div>
              <div className="uk-margin">
                <input placeholder = "Product price"
                type = "text"
                className="uk-input bg-custom-dblue"
                value={ this.state.price }
                onChange={ e =>
                            this.setState(
                              { price: e.target.value.replace(/\D/,'') }
                            )
                          }
                />
              </div>
              <div className="uk-text-center">
                <div>
                  <Counter setCount={this.setCount}
                           value={ this.counter }
                           ref="counter"/>
                  <LazyLoad>
                      <img src={this.state.currentImage}
                           onClick={ !!isEnabled ? this.toggleImageList : null   }
                           className="img-select h-off"/>
                  </LazyLoad>
                  {
                    this.state.toggleImage ?
                    <ImageList handleImage={this.handleImage}/> :
                    null
                  }
                </div>
               </div>
               <div className="uk-text-center">
                 <input onClick = { this.addItem }
                         type="button"
                         className={ "uk-button bg-custom-dblue custom-submit" }
                         value="Add item"
                         disabled={ !isEnabled }
                         onClick={ this.addProduct }/>
               </div>
          </form>
        </div>
      </div>
      );
  }
};

export default connect(
  state => ({
    products: state
  }),
  dispatch => ({
    onAddProduct: (payload={}) => {
      dispatch({ type:  'ADD_PRODUCT', payload: payload })
    }
  })
)(ProductForm);
