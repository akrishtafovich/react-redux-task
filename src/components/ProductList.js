import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Counter from './Counter';
import { itemsFetchData, removeProducts, updateProductCount } from '../actions/products';

class ProductList extends Component {
  componentDidMount() {
    if(this.props.products.length === 0){
      this.props.fetchData('http://5aa3e268a53a880014175310.mockapi.io/api/v1/products/');
    }
  }

  handleRemove = (id, value) => {
    this.props.removeProduct(id);
  }

  setCount = (id, count) => {
    this.props.updateCounter(id, count);
  }

  render() {
    var listItems = this.props.products.map((item) => {
        return (
          <div key={ item.id } className="bg-custom-dblue l-roduct">
              <div className="uk-width-1-3 d-inline image-params">
                <img src={item.image} className="img-select h-off uk-align-left uk-text-top"/>
              </div>
              <div className="uk-width-1-3 d-inline">
                <div className="product-data">
                  <span className="uk-text-meta">{ item.name }</span>
                  <div className="c-product">
                    <Counter value={ item.count }
                             updateCounter={ this.setCount }
                             id={ item.id } />
                  </div>
                  <span className="uk-text-meta">Total: { item.total} $</span>
                </div>
              </div>
              <div className="uk-width-1-3 d-inline uk-align-right uk-margin-remove">
                <div className="uk-align-right uk-margin-remove">
                  <span onClick={ () => this.handleRemove(item.id, item.total) }
                        className="p-img h-danger">
                    <img src={require('../img/trash.png')} />
                  </span>
                  <Link to={ "/" + item.id } key={ item.id } className="p-img h-primary" >
                     <img src={require('../img/tool.png')} />
                  </Link>
                </div>
              </div>
          </div>
        )
    });

    return (
      <div className="column">
        <h3>Product list</h3>
        <div className="product-container">
            { listItems }
        </div>
      </div>
      );
  }
};

  const mapStateToProps = (state) => {
        return {
            products: state.products,

        };
    };
    const mapDispatchToProps = (dispatch) => {
    return{
          fetchData: (url) => dispatch(itemsFetchData(url)),
          removeProduct: (id) => dispatch(removeProducts(id)),
          updateCounter: (id, count) => dispatch(updateProductCount(id, count))
        };
    };

   export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
