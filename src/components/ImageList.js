import React, { Component } from 'react';
import LazyLoad from 'react-lazy-load';

class ImageList extends Component {
  setImage = (img) => {
    this.props.handleImage(img);
  }
  render() {
    let array = ["apple", "cake", "carrot", "fish"];
    const list = array.map((img) => {
      return(
        <img key={img} src={ require('../img/'+img+'.png') }
                       className="img-select"
                       onClick={() => this.setImage(img)}/>
      )
    })
    return (
      <div>
        <LazyLoad className="image-list-container">
          <div>
            { list }
          </div>
        </LazyLoad>
      </div>
    );
  }
}

export default ImageList;
