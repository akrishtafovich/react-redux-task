import React from 'react';
import ReactDOM from 'react-dom';
// import reducer from './reducers';


import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import { ConnectedRouter as Router } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import configureStore from './store/configureStore';


// const store = configureStore();
const history = createHistory();

ReactDOM.render(
  <Provider store={configureStore}>
  <Router history={history}>
      <App/>
  </Router>
  </Provider>, document.getElementById('root'));
registerServiceWorker();
