import axios from 'axios';

export function itemsHaveError(bool) {
        return {
            type: 'ITEMS_HAVE_ERROR',
            hasError: bool
        };
    }

    export function itemsAreLoading(bool) {
        return {
            type: 'ITEMS_ARE_LOADING',
            isLoading: bool
        };
    }

    export function itemsFetchDataSuccess(products) {
      products.map( (el) => {
        el.id = Number(el.id);
        el.total = Number(el.price) * el.count;
        el.image = require('../img/' + el.image);
      });
        return {
            type: 'ITEMS_FETCH_DATA_SUCCESS',
            products
        };
    }

    export function removeProducts(id) {
        return {
          type: 'REMOVE_PRODUCT',
          id
        }
    }

    export function updateProductCount(id, count) {
        return {
          type: 'UPDATE_COUNTER',
          id, count
        }
    }

    export function itemsFetchData(url) {
        return (dispatch) => {
            dispatch(itemsAreLoading(true));
            axios.get(url)
                .then((response) => {
                    if (response.status !== 200) {
                        throw Error(response.statusText);
                    }
                    dispatch(itemsAreLoading(false));
                    return response;
                })
                .then((response) => dispatch(itemsFetchDataSuccess(response.data)))
                .catch(() => dispatch(itemsHaveError(true)));
        };
    }
