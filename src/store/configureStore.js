import { createStore, applyMiddleware } from "redux";
import productsApp from "../reducers";
import thunk from "redux-thunk";

const middleware = [thunk];
const store = createStore(productsApp, applyMiddleware(...middleware));
export default store;
