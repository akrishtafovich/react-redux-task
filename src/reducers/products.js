export function itemsHaveError(state = false, action) {
        switch (action.type) {
            case 'ITEMS_HAVE_ERROR':
                return action.hasError;
            default:
                return state;
        }
    }

    export function itemsAreLoading(state = false, action) {
        switch (action.type) {
            case 'ITEMS_ARE_LOADING':
                return action.isLoading;
            default:
                return state;
        }
    }

    export function productList(state = [], action) {
        switch (action.type) {
            case 'ITEMS_FETCH_DATA_SUCCESS':
                return action.products;
            case 'ADD_PRODUCT':
              return [
                ...state,
                action.payload
              ];
            case 'REMOVE_PRODUCT':
              return state.filter(item => action.id !== item.id);
            case 'UPDATE_COUNTER':
              let index = state.findIndex(item => item.id === action.id);
              let products = state[index];
              products.count = action.count;
              products.total = products.count * products.price;
              return[
                ...state
              ]
            default:
              return state;
        }
      }
